const http = require("http")
const fs = require("fs")

function loadHTML(file, statusCode, res) {
    let html = fs.readFileSync(file)
    let contentType = 'text/html'
    let extension = file.split('.')
    extension = extension[extension.length - 1]
    switch (extension) {
        case 'js':
            contentType = 'application/javascript'
            break
        case 'css':
            contentType = 'text/css'
            break
        case 'png':
            contentType = 'image/png'
            break
        case 'jpg':
        case 'jpeg':
            contentType = 'image/jpg'
            break
        case 'svg':
            contentType = 'text/xml'
            break
    }
    res.writeHead(statusCode, { "content-type": "text/html" })
    res.end(html)
}

function onRequest(req, res) {
    console.log(req.url)
    switch (req.url) {
        case "/":
            loadHTML("public/index.html", 200, res)
            break
        case "/cars":
            loadHTML("public/pages/cari-mobil.html", 200, res)
            break
        default:
            let assets = 'public' + req.url;
            if (fs.existsSync(assets)) {
                loadHTML(assets, 200, res)
            } else
                loadHTML("404.html", 404, res)
            break
    }
}

const server = http.createServer(onRequest)

server.listen(8000)